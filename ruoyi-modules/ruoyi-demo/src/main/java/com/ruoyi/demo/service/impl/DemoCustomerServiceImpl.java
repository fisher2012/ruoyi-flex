package com.ruoyi.demo.service.impl;

import java.util.List;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.demo.domain.DemoGoods;
import com.ruoyi.demo.mapper.DemoCustomerMapper;
import com.ruoyi.demo.domain.DemoCustomer;
import com.ruoyi.demo.service.IDemoCustomerService;

/**
 * 客户主表(mb)Service业务层处理
 * 
 * @author 数据小王子
 * 2023-07-11
 */
@Service
public class DemoCustomerServiceImpl implements IDemoCustomerService 
{
    @Resource
    private DemoCustomerMapper demoCustomerMapper;

    /**
     * 查询客户主表(mb)
     * 
     * @param customerId 客户主表(mb)主键
     * @return 客户主表(mb)
     */
    @Override
    public DemoCustomer selectDemoCustomerByCustomerId(Long customerId)
    {
        return demoCustomerMapper.selectDemoCustomerByCustomerId(customerId);
    }

    /**
     * 查询客户主表(mb)列表
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 客户主表(mb)
     */
    @Override
    public List<DemoCustomer> selectDemoCustomerList(DemoCustomer demoCustomer)
    {
        return demoCustomerMapper.selectDemoCustomerList(demoCustomer);
    }

    /**
     * 新增客户主表(mb)
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    @Transactional
    @Override
    public int insertDemoCustomer(DemoCustomer demoCustomer)
    {
        int rows = demoCustomerMapper.insertDemoCustomer(demoCustomer);
        insertDemoGoods(demoCustomer);
        return rows;
    }

    /**
     * 修改客户主表(mb)
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    @Transactional
    @Override
    public int updateDemoCustomer(DemoCustomer demoCustomer)
    {
        demoCustomerMapper.deleteDemoGoodsByCustomerId(demoCustomer.getCustomerId());
        insertDemoGoods(demoCustomer);
        return demoCustomerMapper.updateDemoCustomer(demoCustomer);
    }

    /**
     * 批量删除客户主表(mb)
     * 
     * @param customerIds 需要删除的客户主表(mb)主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDemoCustomerByCustomerIds(Long[] customerIds)
    {
        demoCustomerMapper.deleteDemoGoodsByCustomerIds(customerIds);
        return demoCustomerMapper.deleteDemoCustomerByCustomerIds(customerIds);
    }

    /**
     * 删除客户主表(mb)信息
     * 
     * @param customerId 客户主表(mb)主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDemoCustomerByCustomerId(Long customerId)
    {
        demoCustomerMapper.deleteDemoGoodsByCustomerId(customerId);
        return demoCustomerMapper.deleteDemoCustomerByCustomerId(customerId);
    }

    /**
     * 新增商品子信息
     * 
     * @param demoCustomer 客户主表(mb)对象
     */
    public void insertDemoGoods(DemoCustomer demoCustomer)
    {
        List<DemoGoods> demoGoodsList = demoCustomer.getDemoGoodsList();
        Long customerId = demoCustomer.getCustomerId();
        if (StringUtils.isNotNull(demoGoodsList))
        {
            List<DemoGoods> list = new ArrayList<>();
            for (DemoGoods demoGoods : demoGoodsList)
            {
                demoGoods.setCustomerId(customerId);
                list.add(demoGoods);
            }
            if (list.size() > 0)
            {
                demoCustomerMapper.batchDemoGoods(list);
            }
        }
    }
}
