package com.ruoyi.demo.controller;

import java.util.List;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.common.orm.core.page.TableDataInfo;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.web.core.BaseController;
import com.ruoyi.common.core.core.domain.AjaxResult;
import com.ruoyi.demo.domain.DemoCustomer;
import com.ruoyi.demo.service.IDemoCustomerService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 客户主表(mb)Controller
 *
 * @author 数据小王子
 * 2023-07-11
 */
@RestController
@RequestMapping("/demo/customer")
public class DemoCustomerController extends BaseController
{
    @Resource
    private IDemoCustomerService demoCustomerService;

    /**
     * 查询客户主表(mb)列表
     */
    @SaCheckPermission("demo:customer:list")
    @GetMapping("/list")
    public TableDataInfo list(DemoCustomer demoCustomer)
    {
        startPage();
        List<DemoCustomer> list = demoCustomerService.selectDemoCustomerList(demoCustomer);
        return getDataTable(list);
    }

    /**
     * 导出客户主表(mb)列表
     */
    @SaCheckPermission("demo:customer:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoCustomer demoCustomer)
    {
        List<DemoCustomer> list = demoCustomerService.selectDemoCustomerList(demoCustomer);
        ExcelUtil<DemoCustomer> util = new ExcelUtil<>(DemoCustomer.class);
        util.exportExcel(response, list, "客户主表(mb)数据");
    }

    /**
     * 获取客户主表(mb)详细信息
     */
    @SaCheckPermission("demo:customer:query")
    @GetMapping(value = "/{customerId}")
    public AjaxResult getInfo(@PathVariable("customerId") Long customerId)
    {
        return success(demoCustomerService.selectDemoCustomerByCustomerId(customerId));
    }

    /**
     * 新增客户主表(mb)
     */
    @SaCheckPermission("demo:customer:add")
    @PostMapping
    public AjaxResult add(@RequestBody DemoCustomer demoCustomer)
    {
        return toAjax(demoCustomerService.insertDemoCustomer(demoCustomer));
    }

    /**
     * 修改客户主表(mb)
     */
    @SaCheckPermission("demo:customer:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody DemoCustomer demoCustomer)
    {
        return toAjax(demoCustomerService.updateDemoCustomer(demoCustomer));
    }

    /**
     * 删除客户主表(mb)
     */
    @SaCheckPermission("demo:customer:remove")
    @DeleteMapping("/{customerIds}")
    public AjaxResult remove(@PathVariable Long[] customerIds)
    {
        return toAjax(demoCustomerService.deleteDemoCustomerByCustomerIds(customerIds));
    }
}
